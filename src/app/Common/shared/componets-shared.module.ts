import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PrimengSharedModule } from './primeng-shared.module';
import { TopMenuComponent } from './top-menu/top-menu.component';


@NgModule({
  declarations: [
    TopMenuComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    PrimengSharedModule
  ],
  exports: [
    TranslateModule,
    TopMenuComponent
  ]
})
export class ComponetsSharedModule { }
