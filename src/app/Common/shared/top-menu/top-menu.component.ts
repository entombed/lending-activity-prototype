import { Component, OnInit, ElementRef } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  items: MenuItem[];

  constructor(
    public elementRef: ElementRef
  ) { }
  ngOnInit() {
    this.items = [
      {
        label: 'Processes',
        icon: 'pi pi-fw pi-plus',
        routerLink: ['/App/processes/exapmle-grid']
      },
      {
        label: 'Settings',
        icon: 'pi pi-fw pi-pencil',
        command: () => { this.showHideLeftMenu()}
      }
    ];
  }

  showHideLeftMenu() {
    // console.log(this.elementRef);
    const menu = document.querySelector('.be-content__left-sidebar');
    const hidden = menu.classList.contains('be-hidden-menu');
    if (hidden) {
      menu.classList.remove('be-hidden-menu');
    } else {
      menu.classList.add('be-hidden-menu');
    }
  }

}
