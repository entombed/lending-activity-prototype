import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { MessageModule } from 'primeng/message';
import { SidebarModule } from 'primeng/sidebar';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { AccordionModule } from 'primeng/accordion';
import { ChartModule } from 'primeng/chart';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { MultiSelectModule } from 'primeng/multiselect';
import { ListboxModule } from 'primeng/listbox';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FileUploadModule } from 'primeng/fileupload';
import { ColorPickerModule } from 'primeng/colorpicker';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TableModule,
    ButtonModule,
    ToastModule,
    MessageModule,
    SidebarModule,
    MenuModule,
    MenubarModule,
    AccordionModule,
    ChartModule,
    RadioButtonModule,
    DropdownModule,
    CheckboxModule,
    CalendarModule,
    TabViewModule,
    InputTextModule,
    ContextMenuModule,
    ToggleButtonModule,
    DialogModule,
    PanelModule,
    TooltipModule,
    MultiSelectModule,
    ListboxModule,
    ProgressSpinnerModule,
    InputTextareaModule,
    FileUploadModule,
    ColorPickerModule
  ],
  exports: [
    TableModule,
    ButtonModule,
    ToastModule,
    MessageModule,
    SidebarModule,
    MenuModule,
    MenubarModule,
    AccordionModule,
    ChartModule,
    RadioButtonModule,
    DropdownModule,
    CheckboxModule,
    CalendarModule,
    TabViewModule,
    InputTextModule,
    ContextMenuModule,
    ToggleButtonModule,
    DialogModule,
    PanelModule,
    TooltipModule,
    MultiSelectModule,
    ListboxModule,
    ProgressSpinnerModule,
    InputTextareaModule,
    FileUploadModule,
    ColorPickerModule
  ],
  providers: [
  ]
})
export class PrimengSharedModule { }
