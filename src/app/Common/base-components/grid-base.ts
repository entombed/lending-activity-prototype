import { ODataServiceBase } from '@beua-services/odata/odataservicebase';
import { ODataQuery } from '@beua-services/odata/odataquery';
import {
  ElementRef,
  ViewChild,
  Output,
  Input,
  EventEmitter,
  AfterViewInit,
  OnDestroy,
  Renderer2,
  OnInit,
  HostListener
} from '@angular/core';
import { Table } from 'primeng/table';
import { GridColumn, SortingIndexes } from './grid-settings';
import { GridController } from './grid-controller';
import { Router } from '@angular/router';
import { NewTabService } from '@beua-services/new-tab.service';
import { Exception } from '@beua/shared/exception';
import { Subscription, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { map, debounceTime, tap } from 'rxjs/operators';
import { MessageInfoService } from '@beua-services/message-info.service';

export type LanguageSuffix = 'NAT' | 'ENG';
export interface GridButton {
  icon: string;
  click: string;
  tooltip: string;
  color?: string;
  disabled?: boolean;
}
export type GridOpenLocation = 'onModal' | 'onCard' | 'onGrid';

export interface ValidationItem {
  value: string | number | Date;
  name: string;
  element: string;
}

export interface EmessItem {
  field: string;
  element: string;
}

export class GridBase<T> extends GridController implements OnInit, AfterViewInit, OnDestroy {

  /* Нужно чтобы получить метод reset для сброса грида*/
  @ViewChild('dataTable', { static: false }) dataTable: Table;

  @Output() gridInitialized: EventEmitter<null> = new EventEmitter();

  // парметры фильтрации
  @Input() paramsFilter: any = null;
  // признак что грид открывается в модальном окне
  @Input() isModal: boolean = false;
  @Input() gridIsOpenOn: GridOpenLocation = 'onGrid';
  public deleteCardVisible: boolean = false; // сообщение при удалении
  public cardVisible: boolean = false; // показать карточку
  public historyGridVisible: boolean = false;

  public filterFirst = false; // сначала фильтр, потом выбор данных
  public showFilter: boolean = false;

  public gridView: Array<T> = [];
  public totalCount: number = null;
  public multiSortMeta: Array<object> = [];
  protected i18nWords: string[] = []; // массив слов для которых необходимо получить перевод
  public translations: {} = {}; // словарик с переведенными словами
  // метод подготовки popup сообщений
  // protected informationMessage: InformationMessage;
  // метод возвращает тектс ошибки
  protected exception: Exception;
  // используется для добавления и последующей отписки при удаленние компонента (this.subscription.add())
  protected subscription: Subscription = new Subscription();
  /* setter при выборе строки грида составляет роут карточки для открытия в новом окне*/
  // public get selectedItem(): Array<T> | T {
  //   return this._selectedItem;
  // }
  // public set selectedItem(item: Array<T> | T) {
  //   this._selectedItem = item;
  //   if (item) {
  //     this.setCardId();
  //   }
  //   this.fullCardRoute = `${this.cardRoute}${this.cardId}`;
  // }

  public selectedItem: Array<T>;

  /*
   * gridRoute роут грида, нужен для открытия вложенного грида в новом окне
   * cardRoute роут карточки
   * fullCardRoute роут карточки с id
   */
  protected fullCardRoute: string = null;
  protected cardRoute: string = null;
  protected gridRoute: string = null;

  public gridName: string = null; // имя для грида
  public gridMenu: Array<GridButton> = []; // меню грида
  // какие кнопки выводятся в меню по умолчанию
  protected menuPresets: { [key: string]: Array<string> } = {
    default: ['new', 'delete', 'edit', 'filter', 'refresh', 'selectAll', 'deSelectAll', 'exportToExcel', 'resetStoredSettings']
  };

  public buttonsDisabledStatus: { [key: string]: boolean } = {
    edit: null,
    new: null,
    delete: null,
    cloneCard: null,
    refresh: null,
    exportToExcel: null,
    resetStoredSettings: null,
    newTab: null,
  };

  /**
   *
   *
   * @private
   * @type {{ [key: string]: GridButton }}
   * @memberof GridBase
   */
  private buttons: { [key: string]: GridButton } = {
    exportToExcel: {
      icon: 'pi pi-file-excel',
      click: 'exportToExcel',
      tooltip: 'TO_EXCEL',
      color: 'green'
    },
    resetStoredSettings: {
      icon: 'pi pi-calendar-times',
      click: 'resetStoredSettings',
      tooltip: 'RESET_SETTINGS',
      color: 'blue'
    },
    edit: {
      icon: 'pi pi-search',
      click: 'edit',
      tooltip: 'VIEW',
      color: 'indigo'
    },
    cloneCard: {
      icon: 'pi pi-clone',
      click: 'cloneCard',
      tooltip: 'CLONE_CARD'
    },
    filter: {
      icon: 'pi pi-filter',
      click: 'filter',
      tooltip: 'SEARCH_PARAM',
      color: 'indigo'
    },
    new: {
      icon: 'pi pi-plus',
      click: 'new',
      tooltip: 'ADD'
    },
    delete: {
      icon: 'pi pi-minus',
      click: 'delete',
      tooltip: 'DELETE'
    },
    refresh: {
      icon: 'pi pi-refresh',
      click: 'refresh',
      tooltip: 'REFRESH_DATA'
    },
    selectAll: {
      icon: 'pi pi-copy',
      click: 'selectAll',
      tooltip: 'SELECT_ALL'
    },
    deSelectAll: {
      icon: 'pi pi-file',
      click: 'deSelectAll',
      tooltip: 'DESELECT'
    },
    newTab: {
      icon: 'pi pi-external-link',
      click: 'newTab',
      tooltip: ''
    },
    history: {
      icon: 'pi pi-info-circle',
      click: 'history',
      tooltip: 'VIEW_CHANGE_HIST',
    }
  };


  public cardId: number; // ID карточки
  protected tableBody: HTMLElement; // тело таблицы
  // protected standartHeight = true; // дефолтное значение высоты для тела таблицы
  protected keyId: string = null; // поле таблицы которое хранит ID, используется для определения cardId
  protected paddingGrid = 290; // высота которая вычисляется от размера экрана и получаем высоту тела таблицы
  // protected fixedHeigth: number = null; // указываем для фиксированной высоты грида
  protected query: ODataQuery<T> = this.service ? this.service.Query() : null; // TODO
  // приставка для добавления к имени поля, когда требуется выводить поле в зависимости от языка интерфейса
  protected langSuffix: LanguageSuffix = null;
  private gridData: Array<T> = [];
  private isFirstLoad: boolean = true;
  public sortingParams: Array<object> = [];
  public initialPaginationParams = { first: 0, rows: 30 };
  private paginationParams = this.initialPaginationParams;
  private sortingOperationsIndexes: Array<SortingIndexes> = [];
  // private _selectedItem: Array<T> | T = null;
  // private subjectShowHint = new Subject();
  protected userService = this.service ? this.service.getUserService() : null; // TODO
  // protected isModal: boolean = false;

  public Emess: EmessItem[] = []; // сообщения валидации полей

  // action для которых допускается что бы не было выбрана запись на гриде
  protected selectedItemCheckExludeList: Array<string> = [
    'resetStoredSettings', 'exportToExcel', 'filter', 'new', 'refresh', 'selectAll', 'deSelectAll', 'newTab', 'history', 'customerReport'
  ];

  // action для которых проверяется что выбрана только одна запись на гриде
  protected singleSelectionItems: string[] = [
    'edit', 'cloneCard'
  ];

  constructor(
    protected elem: ElementRef,
    protected router: Router,
    protected newTabService: NewTabService,
    private service: ODataServiceBase<T>,
    protected messageInfoService?: MessageInfoService,
    protected translateService?: TranslateService,
    protected renderer?: Renderer2
  ) {
    super(newTabService);
    this.setGridCode(this.elem);
    this.exception = new Exception();
    // this.createSubjectShowHint();
    this.langSuffix = this.getCurrentLang();
  }

  ngOnInit() {
    this.fillGridMenu(this.menuPresets.default);
  }

  ngAfterViewInit() {
    this.gridInitialized.emit();
    // if (this.renderer) {
    //   this.getTableBody();
    // }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setTableBodyHeight();
  }

  /**
   * определяем язык интерфейса
   *
   * @author A.Bondarenko
   * @date 2020-04-29
   * @protected
   * @returns {LanguageSuffix}
   * @memberof GridBase
   */
  protected getCurrentLang(): LanguageSuffix {
    let lang = null;
    if (this.translateService) {
      lang = (this.translateService.currentLang === 'ua') ? 'NAT' : 'ENG';
    }
    return lang;
  }

  /**
   * двойной клик на записи в гриде
   *
   * @author A.Bondarenko
   * @date 2020-04-29
   * @param {*} event
   * @param {*} rowData
   * @memberof GridBase
   */
  public rowDblClickHandler(event, rowData): void {
    this.selectedItem = [rowData];
    this.actionHandler('edit');
  }

  /**
   * получаем данные для отображения хинта (из this.subjectShowHint.next)
   * делаем задержку что бы не отображать просто при проведении мышки по значениям таблицы
   * если пользователь задержал указатель, выводим даннеы
   * передаем в метод который отвечает за отображение
   *
   * @author A.Bondarenko
   * @date 2020-04-10
   * @memberof GridBase
   */
  // createSubjectShowHint() {
  //   this.subscription.add(
  //     this.subjectShowHint.pipe(
  //       tap((data) => {
  //         // console.log(data)
  //       })
  //     ).subscribe((data: any) => {
  //       this.showHint(data.event, data.cellData, data.rowData, data.hint);
  //     })
  //   );
  // }


  /**
   * метод вызывается с html при наведении указателя на значение в таблице
   * данные полученные из таблицы передаем прослушивателя
   * формируем их в объект и толкаем в subjectShowHint
   *
   * @author A.Bondarenko
   * @date 2020-04-10
   * @param {*} event - DOM event
   * @param {*} cellData - значенине ячейки
   * @param {*} rowData - значение всей строки таблицы
   * @param {*} hint - имя хинта
   * @memberof GridBase
   */
  // actionSubjectShowHint(event: MouseEvent, cellData: any, rowData: any, hint: string) {
  //   this.subjectShowHint.next({ event, cellData, rowData, hint });
  // }

  /**
   * Загрузка данных. При первой загрузке применяется дефолтная пагинация и сбрасываются настройки грида
   * (нужно в случае если один грид заполняется разными данными). Формируется query, после загрузки обнуляется.
   * @protected
   * @memberof GridBase
   */
  protected loadData(): void {
    this.selectedItem = null;
    this.modifyQuery();
    this.applyFilter(this.query);
    this.applyPagination();
    this.sortQuery(this.sortingParams);

    this.query.WithInlineCount().Exec().then(
      data => {
        this.resetTableSettingsOnFirstLoad();
        this.totalCount = data['@odata.count'];
        this.gridData = data.value;
        this.initGridView();

        this.isFirstLoad = false;
        this.clearQuery();
      }
    )
      .catch(
        error => {
          this.messageInfoService.showErrorMessage(this.exception.parse(error));
          this.clearQuery();
        }
      );
  }

  // Применение пагинации для грида
  private applyPagination(): void {
    if (this.isFirstLoad) {
      this.paginationQuery(this.initialPaginationParams);
    } else {
      this.paginationQuery(this.paginationParams);
    }
  }

  private paginationQuery(paginationParams): void {
    this.query.Skip(paginationParams.first).Top(paginationParams.rows);
  }

  // Сортировка для грида
  private sortQuery(sortParams): void {
    this.query.OrderBy(sortParams);
  }

  private resetTableSettingsOnFirstLoad(): void {
    if (this.isFirstLoad) {
      this.dataTable.reset();
      // this.sortingParams = [];
      // this.paginationParams = this.initialPaginationParams;
    }
  }

  private initGridView(): void {
    this.gridView = this.gridData;
    this.modifyGridView();
  }

  private clearQuery(): void {
    this.query = this.service.Query();
  }

  private clearSortSettings(): void {
    this.sortingOperationsIndexes = [];
    this.sortingParams = [];
    this.multiSortMeta = [];
  }

  /* Перезагрузка грида*/
  public reloadData(): void {
    // this.isFirstLoad = true;
    this.columnsView = this.columns;
    this.loadData();
  }




  /**
   * проверяем какой action выбрал пользователь
   * additionalHandlers - callback в котором содержатся проверки не стандартных action
   *
   * @author A.Bondarenko
   * @date 2020-07-22
   * @param {string} action
   * @param {(action: string) => void} [additionalHandlers]
   * @returns {void}
   * @memberof GridBase
   */
  public actionHandler(action: string, additionalHandlers?: (action: string) => void): void {
    if (this.selectedItem != null && !Array.isArray(this.selectedItem)) {
      this.selectedItem = [this.selectedItem];
    }
    const noSelectedItem = !this.selectedItem || this.selectedItem.length === 0;
    if (!this.isActionExcluded(action) && noSelectedItem) {
      this.messageInfoService.showInfoMessage('MUST_BE_MARKED_ROW');
      return;
    }
    const moreThenOneSelectedItem = this.selectedItem && this.selectedItem.length > 1;
    if (moreThenOneSelectedItem && this.isMultipleSelectionNotAllowed(action)) {
      this.messageInfoService.showInfoMessage('MUST_BE_MARKED_ONE_ROW');
      return;
    }

    if (action === 'edit') {
      this.editCardHandler();
    } else if (action === 'delete') {
      this.deleteCardHandler();
    } else if (action === 'resetStoredSettings') {
      this.resetHandler();
    } else if (action === 'exportToExcel') {
      this.exportToExcelHandler();
    } else if (action === 'cloneCard') {
      this.cloneCardHandler();
    } else if (action === 'filter') {
      this.filterCardHandler();
    } else if (action === 'new') {
      this.newCardHandler();
    } else if (action === 'refresh') {
      this.refreshHandler();
    } else if (action === 'newTab') {
      this.newTabHandler();
    } else if (action === 'selectAll') {
      this.selectAllHandler();
    } else if (action === 'deSelectAll') {
      this.deSelectAllHandler();
    } else if (action === 'history') {
      this.historyGridHandler();
    }

    if (additionalHandlers) {
      additionalHandlers(action);
    }
  }

  /**
   * проверяем action что для него не обязательно выделение
   *
   * @author A.Bondarenko
   * @date 2020-07-22
   * @private
   * @param {string} action
   * @returns {boolean}
   * @memberof GridBase
   */
  private isActionExcluded(action: string): boolean {
    return this.selectedItemCheckExludeList.includes(action);
  }

  /**
   * проверяем action для которых запрещено множественное выделение
   *
   * @author A.Bondarenko
   * @date 2020-07-22
   * @protected
   * @param {string} action
   * @returns {boolean}
   * @memberof GridBase
   */
  protected isMultipleSelectionNotAllowed(action: string): boolean {
    return this.singleSelectionItems.includes(action);
  }

  public paginate(event): void {
    if (this.isPaginationSimilar(event)) {
      return;
    }
    this.paginationParams = event;
    this.selectedItem = null;
    if (this.isSortingApplied()) {
      this.sortQuery(this.sortingParams);
    }
    this.paginationQuery(event);
    this.loadData();
  }

  /**
   * Обработка сортировки, sort срабатывает при первой загрузке грида, поэтому проверяется если ли сортировка вообще.
   * Операции сортировки записываются в массив, если начинают повторятся - сортировка сбрасывается.
   * @param {*} event
   * @returns {void}
   * @memberof GridBase
   */
  public sort(event): void {
    if (!this.isFirstLoad) {
      this.sortingParams = event.multisortmeta;
      if (!this.isSortingApplied()) {
        return;
      }
      this.sortingOperationsIndexes.push(event.multisortmeta[0].order);

      if (this.sortingOperationsIndexes.includes(SortingIndexes.none)) {
        this.clearSortSettings();
        this.applySorting();
        return;
      }
      if (this.sortingOperationsIndexes.includes(SortingIndexes.asc) && this.sortingOperationsIndexes.includes(SortingIndexes.desc)) {
        this.sortingOperationsIndexes.push(SortingIndexes.none);
      }
      this.applySorting();
    }
  }

  protected initColumns(columns: Array<GridColumn>): void {
    this.columns = columns;
    this.applyLocalStorageSettings();
  }

  /**
   * получаем ID выбранной записи по ключу
   * требуется что бы было задано this.keyId]
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @param {*} [keyId=this.keyId]
   * @param {*} [itemCard=this.selectedItem]
   * @memberof GridBase
   */
  protected setCardIdByKey(keyId = this.keyId, itemCard = this.selectedItem): void {
    if (keyId && this.selectedItem && this.selectedItem.length === 1) {
      this.cardId = itemCard[0][keyId];
    }
  }

  /* Переопределяемый метод для изменения query(например фильтр)*/
  protected modifyQuery(): void { }

  protected modifyGridView(): void { }

  protected setService(service: ODataServiceBase<any>): void {
    this.service = service;
    this.query = service.Query();
  }

  // закрываем форму фильтрации, отсылаем объект фильтра на родительский компонент
  public paramsFilterChange(event) {
    this.showFilter = false;
    this.filterFirst = false;
    if (!event.hasOwnProperty('closeForm')) {
      for (const key in this.paramsFilter) {
        // чистим значения в отключенных фильтрах
        if (this.paramsFilter[key]['isFind'] === false) {
          if (this.paramsFilter[key]['value'] ||
            this.paramsFilter[key]['value'] === 0 ||
            this.paramsFilter[key]['value'] === '') { this.paramsFilter[key]['value'] = null; }
          if (this.paramsFilter[key]['desc']) { this.paramsFilter[key]['desc'] = null; }
          if (this.paramsFilter[key]['values']) { this.paramsFilter[key]['values'] = {}; }
          if (this.paramsFilter[key]['valueBegin']) { delete this.paramsFilter[key]['valueBegin']; }
          if (this.paramsFilter[key]['valueEnd']) { delete this.paramsFilter[key]['valueEnd']; }
          if (this.paramsFilter[key]['extValue']) { this.paramsFilter[key]['extValue'] = null; }
          if (this.paramsFilter[key]['items']) { this.paramsFilter[key]['items'] = null; }
        }
      }
      this.paramsFilter = event;
      this.reloadData();
    } else {
      // если мы в модальном режиме и передумали выбирать данные (ни рау не выбирали их)
      // - сразу на фильтре нажали Отменить - закрыть форму
      // if (!this.isGetData && this.modalMode) {
      //     this.toggleFilterVisibility();
      // }
    }
  }

  protected applyFilter(query: ODataQuery<T>) {

  }

  private isPaginationSimilar(event: object): boolean {
    return Object.entries(event).toString() === Object.entries(this.paginationParams).toString();
  }

  private isSortingApplied(): boolean {
    return this.sortingParams.length > 0;
  }

  private applySorting(): void {
    this.sortQuery(this.sortingParams);
    this.paginationQuery(this.paginationParams);
    this.loadData();
  }

  private exportToExcelHandler(): void {
    this.exportToExcel();
  }

  /**
   * открываем карточку в новом окне
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @memberof GridBase
   */
  protected cloneCardHandler(): void {
    // this.openCardNewTab();
    this.setCardIdByKey();
    this.fullCardRoute = `${this.cardRoute}${this.cardId}`;
    this.newTabService.openInNewTab(this.fullCardRoute);
  }

  private resetHandler(): void {
    this.clearStoredSettings();
  }

  /**
   * выводим предупреждение при удалении записи
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @memberof GridBase
   */
  protected deleteCardHandler() {
    this.deleteCardVisible = true;
  }

  /**
   * открываем запись на просмотр, редактирование
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @memberof GridBase
   */
  protected editCardHandler(): void {
    this.setCardIdByKey();
    this.toogleCardVisibility();
  }

  protected filterCardHandler(): void {
    this.toggleFilterVisibility();
  }

  /**
   * создаем новую запись на гриде
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @memberof GridBase
   */
  protected newCardHandler(): void {
    this.cardId = null;
    this.toogleCardVisibility();
  }

  private refreshHandler(): void {
    this.reloadData();
  }

  private selectAllHandler() {
    this.selectedItem = this.gridView;
  }

  private deSelectAllHandler() {
    this.selectedItem = [];
  }

  /**
   * открываем грид в новом окне
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @private
   * @memberof GridBase
   */
  private newTabHandler(): void {
    this.openInNewTab();
  }

  private historyGridHandler(): void {
    this.toogleHistoryGridVisibility();
  }

  /**
   * заглушка выполняется после получения подстверждения о удалении
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @memberof GridBase
   */
  public setCardDeteted(): void {

  }

  public toogleCardVisibility(): void {
    this.cardVisible = !this.cardVisible;
  }

  public toggleFilterVisibility(): void {
    this.showFilter = !this.showFilter;
  }

  public toogleHistoryGridVisibility(): void {
    this.historyGridVisible = !this.historyGridVisible;
  }

  // /**
  //  * открытие карточки в новом окне
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-04-30
  //  * @private
  //  * @memberof GridBase
  //  */
  // private openCardNewTab() {
  //   this.setCardIdByKey();
  //   this.fullCardRoute = `${this.cardRoute}${this.cardId}`;
  //   this.newTabService.openInNewTab(this.fullCardRoute);
  // }

  private exportToExcel(): void { // не работает
    const exportData = this.getDataForExport();
    if (this.selectedItem && this.selectedItem.length > 0) {
      exportData.data = this.selectedItem;
    }
    let query = this.service.PostQuery(exportData);
    this.modifyQuery();
    query.WhereParams({ export: 'Excel' });
    query = query.Top(0);
    query.Exec().then(() => {
      this.getExcelFile(exportData);
    });
  }

  private getExcelFile(exportData): void {
    const url = this.service.getExportUrl(exportData.fileName);
    const anchor = document.createElement('a');
    anchor.setAttribute('href', url);
    anchor.setAttribute('download', exportData.fileName);
    document.body.appendChild(anchor);
    anchor.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(anchor);
  }

  private getDataForExport(): any {
    if (!this.gridName) {
      this.gridName = 'excel';  // вообще говоря gridName всегда должно быть заполнено
    } else {
      this.gridName = this.userService.getTranslateService().instant(this.gridName);
    }
    const sessionId = this.userService.getSessionId();
    const exportData = {
      caption: this.gridName,
      fileName: `${sessionId.toString()}-${this.gridName}.xlsx`,
      columns: []
    };

    this.columns.forEach(column => {
      exportData.columns.push({
        name: this.userService.getTranslateService().instant(column.name),
        field: column.field,
        textAlign: column.textAlign,
        type: column.type
      });
    });
    return exportData;
  }

  private clearStoredSettings(): void {
    localStorage.removeItem(this.localStorageKey);
  }

  private openInNewTab(): void {
    const currentRoute = this.gridRoute ? this.gridRoute : this.router.url;
    this.newTabService.openInNewTab(currentRoute);
  }

  /**
   * получение переводов
   * отправляем массив слов
   * получаем объект, переводы хранятся в this.translations
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string[]} [arrayWords=this.i18nWords] - массив слов которые необходимо перевести
   * @param {(() => void)} [callBack] - callback функция
   * @memberof GridSettings
   */
  protected getTranslates(arrayWords: string[] = this.i18nWords) {
    return this.translateService.get(arrayWords).pipe(
      map((translatedWords) => {
        this.translations = translatedWords;
        return translatedWords;
      })
    );
  }


  /**
   * вывод данные в таблицу грида
   * используется когда данные вложены
   *
   * @author A.Bondarenko
   * @date 2020-04-10
   * @protected
   * @param {*} rowData
   * @param {*} field
   * @returns
   * @memberof GridBase
   */
  public showData(rowData, field) {
    if (field) {
      const arr = field.split('.');
      let res = rowData[arr[0]];
      for (let i = 1, max = arr.length; i < max; i++) {
        (res && res.hasOwnProperty(arr[i])) ? res = res[arr[i]] : res = null;
      }
      return res;
    } else {
      return '';
    }
  }

  /**
   * отображаем span элемент в котором выводим значение хинта
   * находим span элемент в котором будет выводиться хинт
   * добавляем стили к нему
   * выводим текст
   * this.getHint(cellData, rowData, colHint) - возвращает текст подсказки
   *
   * @author A.Bondarenko
   * @date 2020-04-10
   * @param {MouseEvent} event
   * @param {*} cellData
   * @param {*} rowData
   * @param {string} colHint
   * @memberof GridBase
   */
  public showHint(event: MouseEvent, cellData: any, rowData: any, colHint: string) {
    const target = event.target as HTMLSpanElement;
    const hintElement = target.firstElementChild as HTMLSpanElement;
    if (hintElement && hintElement.classList.contains('be-table__cell__hint')) {
      hintElement.style.top = (target.getBoundingClientRect().top + 20) + 'px';
      hintElement.style.left = (target.getBoundingClientRect().left + 20) + 'px';
      hintElement.style.display = 'block';
      if (!hintElement.textContent) {
        hintElement.textContent = this.getHint(cellData, rowData, colHint);
      }
    }
  }

  /**
   * прячем хинт если убрали мышку с значения ячейки
   *
   * @author A.Bondarenko
   * @date 2020-04-10
   * @param {MouseEvent} event
   * @memberof GridBase
   */
  public hideHint(event: MouseEvent) {
    const target = event.target as HTMLSpanElement;
    const hintElement = target.firstElementChild as HTMLSpanElement;
    if (hintElement && hintElement.classList.contains('be-table__cell__hint')) {
      hintElement.style.display = 'none';
    }
  }

  /**
   * заглушка для обработки финта
   * необходимо перебивать компонентах
   *
   * @author A.Bondarenko
   * @date 2020-04-10
   * @protected
   * @param {*} cellData
   * @param {*} rowData
   * @param {*} colHint
   * @returns
   * @memberof GridBase
   */
  protected getHint(cellData, rowData, colHint) {
    return 'used getHint from grid-base...';
  }

  /**
   * заглушка для метода изменение цвета в строке грида
   *
   * @author A.Bondarenko
   * @date 2020-04-21
   * @param {*} rowData
   * @returns {(string | void)}
   * @memberof GridBase
   */
  public setRowColor(rowData): string | void {
    return null;
  }

  /**
   * выставляем высоту грида в зависимости где отображается (gridIsOpenOn)
   * в модольном окне, на карточке или самостоятельный грид
   *
   * @author A.Bondarenko
   * @date 2020-05-19
   * @protected
   * @memberof GridBase
   */
  protected setTableBodyHeight() {
    let tableHeight = null;
    const windowHight = window.innerHeight;
    if (this.gridIsOpenOn === 'onModal') {
      tableHeight = windowHight - 400;
    } else if (this.gridIsOpenOn === 'onCard') {
      tableHeight = 300;
    } else {
      tableHeight = windowHight - this.paddingGrid;
    }
    this.tableBody.style.maxHeight = `${tableHeight}.px`;
    this.tableBody.style.height = `${tableHeight}.px`;
  }

  /**
   * получаем DOM елемент грида (isReady с миксина отправляет этот елемент)
   * выбираем тело грида
   * запускаем метод устанавливающий высоту тела грида
   *
   * @author A.Bondarenko
   * @date 2020-05-19
   * @protected
   * @param {*} table
   * @memberof GridBase
   */
  public getTableBody(table) {
    const currentTable = table.nativeElement;
    this.tableBody = currentTable.querySelector('.ui-table-scrollable-body');
    this.setTableBodyHeight();
  }

  /**
   * метод для работы с пунктами меню
   * для удаляния пунктов меню используем параметр click
   * для добавления пунтов передаем массив с описанием кнопок
   *
   *
   * @author A.Bondarenko
   * @date 2020-04-24
   * @protected
   * @param {string[]} excludeItems - значение ключа click кнопок которые необходимо убрать из списка
   * @param {GridButton[]} [additionalButtons=[]] - кнопки которые необходимо добавить;
   * @memberof GridSettings
   */
  protected reorderGridMenu(excludeItems: string[], additionalButtons: GridButton[] = []) {
    this.gridMenu = this.gridMenu.reduce((accumulator, currentValue) => {
      if (!excludeItems.includes(currentValue.click)) {
        accumulator.push(currentValue);
      }
      return accumulator;
    }, []).concat(additionalButtons);
  }

  /**
 * получаем писок выделенных записей на гриде
 * используется для массового удаления записей или других массовых операций
 * возвращает строку содержащую ID записей, разделенные запятой.
 *
 * @author A.Bondarenko
 * @date 2019-11-22
 * @protected
 * @param {string} name - имя поля которое содержит ID
 * @param {*} [cards=this.selectedItem] - выделенные карточки (по умолчанию это this.selectedItem)
 * @returns {string} - строка содержащая ID записей, разделенные запятой
 * @memberof GridSettings
 */
  protected getIds(name: string, cards = this.selectedItem): string {
    cards = !Array.isArray(cards) ? [cards] : cards;
    const ids: string = cards.map((item) => {
      return item[name];
    }).join(',');
    return ids;
  }

  /**
   * инициируем дефолтноем меню для грида
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @param {string[]} menuSettings
   * @memberof GridBase
   */
  protected fillGridMenu(menuSettings: string[]): void {
    menuSettings.forEach(buttonName => {
      if (this.buttons[buttonName]) {
        this.gridMenu.push(this.buttons[buttonName]);
      }
    });
  }

  /**
   * включение или отключение кнопок грида
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @param {Array<string>} disabledButtons
   * @param {boolean} [disabled=true]
   * @memberof GridBase
   */
  protected setButtonsDisabledStatus(disabledButtons: Array<string>, disabled: boolean = true): void {
    disabledButtons.forEach(button => {
      if (this.buttonsDisabledStatus.hasOwnProperty(button)) {
        this.buttonsDisabledStatus[button] = disabled;
      }
    });
  }

  /**
   * для добавления статуса дополнительных кнопок грида
   *
   * @author A.Bondarenko
   * @date 2020-04-24
   * @protected
   * @param {{}} objectStatus
   * @memberof GridBase
   */
  protected assignButtonsDisabledStatus(objectStatus: {}): void {
    Object.assign(this.buttonsDisabledStatus, objectStatus);
  }

  protected setGridName(name: string): void {
    this.gridName = name;
  }

  /**
   * закрываем карточку, если требуется перегружаем грид
   *
   * @author A.Bondarenko
   * @date 2020-05-05
   * @param {*} event
   * @memberof GridBase
   */
  public cardClosedHandler(event) {
    this.toogleCardVisibility();
    if (event && event.reload) {
      this.reloadData();
    }
  }

  /**
   * проверяем поле и что оно содержит запись
   * формируем массив ошибок валидации
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @protected
   * @param {*} value
   * @param {*} name
   * @param {*} element
   * @memberof CardBase
   */
  protected addRequiredError(value, name, element) {
    let exists = [];
    exists = this.Emess.filter((item) => {
      return item.field === name && item.element === element;
    });
    if (exists.length === 0) {
      if (value == null) {
        this.Emess.push({ field: name, element: element });
      } else if (typeof value === 'number' && isNaN(value)) {
        this.Emess.push({ field: name, element: element });
      } else if (typeof value === 'string' && value === '') {
        this.Emess.push({ field: name, element: element });
      }
    }
  }

  /**
   * проверяем что поля форм обязательных к заполнению, заполнены
   * передаем массив объектов
   * если валидация не прошла не блокируем поля;
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @protected
   * @param {ValidationItem[]} fields
   * @memberof CardBase
   */
  protected checkCorrectnessFields(fields: ValidationItem[]) {
    fields.forEach(item => this.addRequiredError(item.value, item.name, item.element));
  }

  /**
   * переводим фокус если кликнули за запись, из списка ошибок валидации
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @param {*} element
   * @memberof CardBase
   */
  public goToElement(element): void {
    // фокус можно поставить только на ui-inputtext
    if (element) {
      const domElement = document.querySelector('#' + element);
      if (domElement && domElement.classList.contains('ui-inputtext')) {
        document.getElementById(element).focus();
      }
    }
  }

}
