import { GridSettings, GridColumn, GridLocalStorageSettings, GridColumnSettings } from './grid-settings';
import { HostListener } from '@angular/core';
import { NewTabService } from '@beua-services/new-tab.service';

export class GridController extends GridSettings {

  /* колонки что отображает грид*/
  public columnsView: Array<GridColumn> = [];

  /* setter сохраняет колонки и заполняет список переключения видимости колонок*/
  public get columns(): Array<GridColumn> {
    return this._columns;
  }
  public set columns(cols: Array<GridColumn>) {
    this._columns = cols;
    this.columnsView = this._columns;
    this.fillColumnSelectorOptions(this.columnsView);
  }

  public columnSelectorOptions = [];
  public selectedColumns = [];



  /* колонки грида при инициализации*/
  private _columns: Array<GridColumn> = [];

  private gridLocalStorageSettings: GridLocalStorageSettings = null;

  constructor(
    protected newTabService: NewTabService
  ) {
    super(newTabService);
  }

  /**
   * Разблокировка кнопки редактирование при выборе строки грида
   * @param {*} event
   * @memberof GridController
   */
  public rowSelectHandler(event): void {
    // this.buttonsDisabledStatus.edit = false;
  }

  public rowUnselectHandler(event): void {
    // this.buttonsDisabledStatus.edit = true;
  }

  public toogleColumnSelector(): void {
    this.columnSelectorVisible = !this.columnSelectorVisible;
  }

  /**
   * Обработка ресайза колонок, вызывается с пага
   * @param {*} event
   * @memberof GridController
   */
  public colResize(event): void {
    this.applyColWidth(event);
    this.setLocalStorageSettings();
  }

  // Обработка перестановки колонок местами, вызывается с пага
  public colReorder(event): void {
    const colOrder = event.columns.map((col, colIndex) => Object({ name: col.name, index: colIndex }));

    colOrder.forEach(col => {
      const localStorageCol = this.getLocalStorageColByName(col.name);
      if (localStorageCol) {
        localStorageCol.orderIndex = col.index;
      }
    });
    this.setLocalStorageSettings();
  }

  /**
   * Обработка переключения видимости колонок
   * @param {*} event
   * @memberof GridController
   */
  public columnSelectorChange(event): void {
    this.columnsView.forEach(colView => {
      colView.visible = this.selectedColumns.some(selectedCol => colView.name === selectedCol);

      const localStorageCol = this.getLocalStorageColByName(colView.name);
      if (localStorageCol) {
        localStorageCol.visible = colView.visible;
      }
    });
    this.setLocalStorageSettings();
  }

  // М-д вызывается при инициализации грида
  /* Получает и применяет настройки грида из ls */
  protected applyLocalStorageSettings(): void {
    this.gridLocalStorageSettings = this.getLocalStorageSettings();

    this.applyLocalStorageSettingsOnColumnView();
    this.columnsView.sort((a, b) => a.orderIndex - b.orderIndex);
    this.fillSelectedColumns();
  }

  // Если в ЛокалСторидж нет записи для этого грида, то настройки беруться из свойст экземпляра класса
  private getLocalStorageSettings(): GridLocalStorageSettings {
    const settings = localStorage.getItem(this.localStorageKey);
    return settings ? JSON.parse(settings) : this.formLocalStorageSettings(this.columns);
  }

  // Отображение колонок грида согласно настроек из Локал-сторидж
  // Сравнение колонок в Локал-сторидж и во вью
  private applyLocalStorageSettingsOnColumnView(): void {
    this.columnsView.forEach((col, index) => {
      const colExist = this.gridLocalStorageSettings.columns.find(el => el.name === col.name);
      if (colExist) {
        col.width = colExist.width;
        col.visible = colExist.visible;
        if (!isNaN(colExist.orderIndex)) {
          col.orderIndex = colExist.orderIndex;
        } else {
          col.orderIndex = index;
        }
      } else {
        // Если настройки для данного грида уже были в локал-сторидж, но колонок на гриде
        // стало больше (добавились новые), добавляем их в соотв. обьект
        col.orderIndex = index + 999;
        this.gridLocalStorageSettings.columns.push(
          {
            name: col.name, width: col.width, orderIndex: col.orderIndex, visible: col.visible
          }
        );
      }
    });
    // Если в экземпляре класса колонок стало меньше чем сохраненно в Локал-сторидж,
    // очищаем локал сторидж от таких колонок
    let viewAndLsEqual = true;
    for (let i = this.gridLocalStorageSettings.columns.length - 1; i >= 0; i-- ) {
      const colWasDeletedFromView = !this.columnsView.find(el => el.name === this.gridLocalStorageSettings.columns[i].name);
      if (colWasDeletedFromView) {
        this.gridLocalStorageSettings.columns.splice(i, 1);
        viewAndLsEqual = false;
      }
    }
    if (!viewAndLsEqual) {
      this.setLocalStorageSettings();
    }
  }

  /* Заполнение переключателя видимости колонок отображаемыми колонками*/
  private fillSelectedColumns(): void {
    this.gridLocalStorageSettings.columns.forEach(colSetting => {
      if (colSetting.visible) {
        this.selectedColumns.push(colSetting.name);
      }
    });
  }

  /**
   * складывает разницу ресайза колонки с ее видимой шириной, записывает в переменную с настройками ls
   * @private
   * @param {*} event
   * @memberof GridController
   */
  private applyColWidth(event): void {
    const columnId = event.element.id;
    const columnView = this.columnsView.find(colView => colView.name === columnId);

    const columnResizedWidthRounded = Math.round(event.delta);
    const clientWidthRounded = Math.round(event.element.clientWidth);
    const columnViewWidthRounded = columnView.width.includes('%') ? clientWidthRounded : Math.round(parseInt(columnView.width, 10));

    const widthSum = columnViewWidthRounded + columnResizedWidthRounded;
    const columnWidth = `${(widthSum < 100 ? 100 : widthSum).toString()}px`;
    columnView.width = columnWidth;

    const colExistInLs = this.getLocalStorageColByName(columnId);
    if (colExistInLs) {
      this.getLocalStorageColByName(columnId).width = columnWidth;
    }
  }

  private applyColumnViewWidth(): void {
    this.columnsView.forEach(viewCol =>
      viewCol.width = this.getLocalStorageColByName(viewCol.name).width
    );
  }

  /* записывает настройки в ls*/
  private setLocalStorageSettings(): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.gridLocalStorageSettings));
  }

  private formLocalStorageSettings(columns: Array<GridColumn>): GridLocalStorageSettings {
    const settings = { columns: [] };
    settings.columns = columns.map(col =>
      Object({ name: col.name, width: col.width, orderIndex: col.orderIndex, visible: col.visible })
    );
    return settings;
  }

  private getLocalStorageColByName(name: string): GridColumnSettings {
    return this.gridLocalStorageSettings.columns.find(localStorageCol => localStorageCol.name === name);
  }

  private fillColumnSelectorOptions(cols: Array<GridColumn>): void {
    this.columnSelectorOptions = cols.map(col => Object({ label: col.name, value: col.name }));
  }

}
