import { OnInit, HostListener, Output, EventEmitter, ElementRef, Renderer2, AfterViewInit, Input, OnDestroy } from '@angular/core';
import { AnimationEvent } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Exception } from '@beua/shared/exception';
import { Subscription } from 'rxjs';
import { MessageInfoService } from '@beua-services/message-info.service';
import { DictionaryClient } from '@beua-services/web-api/pcm/pcm.web-api.services';
import { DictionaryClientWithCache } from '@beua-services/dictionary.service';
import { CalendarLang } from '@beua/shared/calendar-lang';

export interface CardFlags {
  editingModeOff: boolean;
  editingModeOn: boolean;
  isNewRecord: boolean;
  isHistoryVisible: boolean;
  isHistoryBlockVisible: boolean;
  options?: object;
}

export interface MenuButton {
  icon: string;
  click: string;
  tooltip: string;
  color?: string;
  disabled?: boolean;
}

export interface ValidationItem {
  value: string | number | Date;
  name: string;
  element: string;
}

export interface EmessItem {
  field: string;
  element: string;
}

export class CardBase implements OnInit, AfterViewInit, OnDestroy {

  @Input() cardId: number = null; // ID карточки
  @Input() additive = ''; // примесь для формирования уникальных ID в DOM
  @Output() cardClosed: EventEmitter<any> = new EventEmitter<any>();

  // public animatoState = 'visible'; // указываем состояние анимации карточки
  public flags: CardFlags = {
    editingModeOff: true, // карточка находится в режиме блокировки (true поля заблокированы)
    editingModeOn: false, // карточка в режиме редактирования (true поля разаблокированы)
    isNewRecord: false, // карточка в режиме создания новой записи или редактирования существующей
    isHistoryVisible: true, // отображать кнопку для просмотра истории в верхнем меню
    isHistoryBlockVisible: true, // отображать кнопку для просмотра истории в блоках
  };
  public additionalButtons: MenuButton[] = []; // дополнительные кнопки для верхнего меню карточки
  public showTopMenuButtons = true; // отображать в шапке меню карточки
  public showBlockMenuButtons = true; // отображать в блоке меню карточки
  public cardName = ''; // имя карточки
  public Emess: EmessItem[] = []; // сообщения валидации полей
  public dictionaries: object[] = []; // словари для выпадающих списков
  protected bodyElement: HTMLBodyElement; // сохраняем body
  protected countModalWindows: number; // кол-во открытых модальных окон в body
  public currentCard = null; // данные полученные с сервера по ID карточки
  public historyVisible = false;
  // public showIsSaved = false;
  public showCloseWindow = false; // окно предупреждение при закрытии карточки когда она в режиме редактирования
  public showCardContent = false; // флаг отвечающий за тображение формы карточки
  public statusCollapse = false; // свернуть или развернуть блоки по кнопке
  protected reloadParentGrid = false; // перегрузить грид после закрытия карточки или нет
  protected cardState: 'onGrid' | 'newTab';
  // метод возвращает тектс ошибки
  protected exception: Exception;
  // используется для добавления и последующей отписки при удаленние компонента (this.subscription.add())
  protected i18nWords: string[] = []; // массив слов для которых необходимо получить перевод
  protected translations: {} = {}; // словарик с переведенными словами
  protected subscription: Subscription = new Subscription();
  protected lang: string; // язык интерфейса
  public calendarLang: any;
  public currentLang: any; // язык для календаря

  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageInfoService: MessageInfoService,
    protected translateService: TranslateService,
    protected router: Router,
    protected dictionaryClient?: DictionaryClient,
  ) {
    this.exception = new Exception();
    this.lang = this.translateService.currentLang; // запоминаем язык интерфейса пользователя

    this.calendarLang = new CalendarLang();
    this.currentLang = this.calendarLang[this.lang]; // язык для календаря
  }

  ngOnInit() {
    if (!this.cardId) {
      this.flags.editingModeOff = false;
      this.flags.editingModeOn = true;
      this.flags.isNewRecord = true;
      this.setVisibleHistoryButton(false);
      this.setCardName('CREATE_ROW');
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  protected listDictionary(list: string, withoutCode: boolean = false, withoutFirstEmptyRow: boolean = false, success?: () => void) {
    (this.dictionaryClient as DictionaryClientWithCache).listDictionary(
      this.dictionaries,
      list,
      withoutCode,
      withoutFirstEmptyRow,
      success
    );
  }

  /**
   * сохраням в переменную body
   * подсчитываем кол-во открытых карточек в момент создания компонента
   *
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  ngAfterViewInit() {
    this.bodyElement = this.renderer.selectRootElement('body', true);
    this.countModalWindows = this.bodyElement.querySelectorAll('.be-modal-card-overlay').length;
    this.disableBodyScroll();
  }

  /**
   * отлавливаем нажатие ESC
   * смотрим у какого компонента на странице кол-во карточек === 1
   * считаем что это самая последняя открытая карточка, запускаем закрытие этой карточки
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {KeyboardEvent} event
   * @memberof BaseCardComponent
   */
  @HostListener('document:keydown.Escape', ['$event'])
  onEscapePress(event: KeyboardEvent) {
    const modal = this.elementRef.nativeElement.querySelectorAll('.be-modal-overlay').length;
    if (modal === 1) {
      this.startCloseCard();
    }
  }

  /**
   *
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string[]} [arrayWords=this.i18nWords] - массив слов которые необходимо перевести
   * @param {(() => void)} [callBack] - callback функция
   * @memberof BaseCardComponent
   */
  protected getTranslates(arrayWords: string[] = this.i18nWords) {
    return this.translateService.get(arrayWords).pipe(
      map((translatedWords) => {
        this.translations = translatedWords;
        return translatedWords;
      })
    );
  }

  /**
   * действия для дополнительных кнопок меню
   *
   * @author A.Bondarenko
   * @date 2020-07-04
   * @param {*} action
   * @memberof CardBase
   */
  public actionHandler(action) {}

  /**
   * отключаем скрол на body
   * для того что бы предотвратить появления второго скрола
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @private
   * @memberof BaseCardComponent
   */
  private disableBodyScroll() {
    // if (this.bodyElement.style.overflow !== 'hidden') {
    //   this.renderer.setStyle(this.bodyElement, 'overflow-y', 'hidden');
    // }
  }

  /**
   * закрываем карточку,
   * если это будет последняя открытая карточка
   * возвращяем body скрол
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} [event]
   * @memberof BaseCardComponent
   */
  public setCardClosed(event?) {
    // if (this.countModalWindows === 1) {
    //   this.renderer.removeStyle(this.bodyElement, 'overflow-y');
    // }
    if (this.cardState === 'newTab') {
      this.router.navigate(['/']);
      return;
    }
    this.cardClosed.emit({ reload: this.reloadParentGrid });
  }

  /**
   * при закрытии карточки смотрим,
   * если она в режиме редактирования выводим окно предупреждение
   * если она не в режиме редактирования запускаем анимацию
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  startCloseCard() {
    if (this.flags.editingModeOff) {
      // this.changeAnimationState('close'); // анимация закрытия
      this.setCardClosed(); // закрытие без анимации
    } else {
      this.showCloseWindow = true;
    }
  }

  // /**
  //  * изменяем состояния анимации
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @param {*} state
  //  * @memberof BaseCardComponent
  //  */
  // public changeAnimationState(state) {
  //   this.animatoState = state;
  // }

  /**
   * обрабатываем нажатие кнопки сохранить
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  public saveCard() {

  }

  /**
   * запрашиваем по новой данные карточки с сервера
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  public renewCard() {

  }

  /**
   * закрываем все открытые акардионы на странице
   * выбираем все открытыте акардионы и закрываем их
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  public collapseOpennedAccordions(event) {
    const setCollapse = event.checked;
    let listAccordion = null;
    if (setCollapse) {
      listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header.ui-state-active > a');
    } else {
      listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header:not(.ui-state-active) > a');
    }
    listAccordion.forEach((element: HTMLElement) => {
      this.renderer.selectRootElement(element, true).click();
    });
  }

  /**
   * делаем поля формы доступными для редактирования или нет
   * true - блокируем
   * false - разблокируем
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} mode
   * @memberof BaseCardComponent
   */
  public setLockFields(mode) {
    this.flags.editingModeOff = mode;
    this.flags.editingModeOn = !mode;
  }

  /**
   * отображаем или прячем кнопку "история изменений"
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @protected
   * @param {boolean} [mode=true]
   * @memberof CardBase
   */
  protected setVisibleHistoryButton(mode: boolean = true): void {
    this.flags.isHistoryVisible = mode;
  }

  /**
   * запускаем после завершения анимации
   * закрытие карточки
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} event
   * @memberof BaseCardComponent
   */
  // public animationDone(event: AnimationEvent) {
  //   if (event.toState === 'close') {
  //     this.setCardClosed(event);
  //   }
  // }

  /**
   * переводим фокус если кликнули за запись, из списка ошибок валидации
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @param {*} element
   * @memberof CardBase
   */
  public goToElement(elementId): void {
    // фокус можно поставить только на ui-inputtext
    // if (element) {
    //   const domElement = document.querySelector('#' + elementId);
    //   if (domElement && domElement.classList.contains('ui-inputtext')) {
    //     document.getElementById(element).focus();
    //   }
    // }
    if (elementId) {
      let el = null;
      try {
        el = this.renderer.selectRootElement(`#${elementId}${this.additive}`, true);
        el.focus();
      } catch {
        console.warn('DOM element by ID did not found');
      }
    }
  }

  /**
   * проверяем поле и что оно содержит запись
   * формируем массив ошибок валидации
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @protected
   * @param {*} value
   * @param {*} name
   * @param {*} element
   * @memberof CardBase
   */
  protected addRequiredError(value, name, element) {
    let exists = [];
    exists = this.Emess.filter((item) => {
      return item.field === name && item.element === element;
    });
    if (exists.length === 0) {
      if (value == null) {
        this.Emess.push({ field: name, element: element });
      } else if (typeof value === 'number' && isNaN(value)) {
        this.Emess.push({ field: name, element: element });
      } else if (typeof value === 'string' && value === '') {
        this.Emess.push({ field: name, element: element });
      }
    }
  }

  /**
   * проверяем что поля форм обязательных к заполнению, заполнены
   * передаем массив объектов
   * если валидация не прошла не блокируем поля;
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @protected
   * @param {ValidationItem[]} fields
   * @memberof CardBase
   */
  protected checkCorrectnessFields(fields: ValidationItem[]) {
    fields.forEach(item => this.addRequiredError(item.value, item.name, item.element));
    if (this.Emess.length > 0) {
      this.setLockFields(false);
    }
  }

  /**
   * задаем имя карточки
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @protected
   * @param {string} name
   * @memberof CardBase
   */
  protected setCardName(name: string): void {
    this.cardName = name;
  }

  /**
   * показываем компонент история изменений
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @memberof CardBase
   */
  public showHisrotyCardComponent() {
    this.historyVisible = true;
  }

  /**
   * прячем компонент история изменений
   *
   * @author A.Bondarenko
   * @date 2020-05-04
   * @memberof CardBase
   */
  public hideHisrotyCardComponent() {
    this.historyVisible = false;
  }

}
