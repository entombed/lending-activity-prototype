import { ElementRef, Output, EventEmitter } from '@angular/core';
import { NewTabService } from '@beua-services/new-tab.service';

export type ColumnType = 'text' | 'number' | 'date' | 'datetime' | 'money' | 'round' | 'func' | 'checkboxYN' | 'color';
export type SelectionModeGrid = 'single' | 'multiple';
// export type SortMode = 'single' | 'multiple';

export interface GridColumn {
  name: string;
  field: string;
  type?: ColumnType;
  header?: string;
  textAlign?: string;
  width?: string;
  visible?: boolean;
  orderIndex?: number;
  hint?: string;
  precision?: number;
  decimal?: string;
  accuracu?: number;
  noZerro?: false;
  /** Метод получения значения для авторасчитываемых колонок (тип )*/
  func?: (...args) => string;
  /** Перечень аргументов для метода func */
  funcParam?: any[];
  orderByClient?: boolean;
}

export interface GridColumnSettings {
  name: string;
  width: string;
  orderIndex: number;
  visible: boolean;
}

export interface GridLocalStorageSettings {
  columns: Array<GridColumnSettings>;
}

// export interface GridButton {
//   icon: string;
//   click: string;
//   tooltip: string;
//   color?: string;
//   disabled?: boolean;
// }

export enum SortingIndexes {
  none = 0,
  asc = 1,
  desc = -1
}

export interface CardOpenedEvent {
  state: 'onGrid' | 'newTab';
  data?: any;
}

export class GridSettings {

  @Output() cardOpened: EventEmitter<CardOpenedEvent> = new EventEmitter();

  public columnSelectorVisible: boolean = false;

  // public gridName: string = null;
  public gridCode: string = null;
  // public gridMenu: Array<GridButton> = [];

  // public selectedCards: any;

  // public get cardId(): number {
  //   return this._cardId;
  // }
  // public set cardId(id: number) {
  //   this._cardId = id;
  //   this.fullCardRoute = `${this.cardRoute}${id}`;
  // }

  public isSrollable: boolean = true;
  public selectionMode: SelectionModeGrid = 'multiple';
  // public sortMode: SortMode = 'multiple';

  // public gridSettings = {
  //   rows: 12
  // };
  /* gridResizeMode режим отображения и поведения колонок грида при ресайзе колонок.
   * Использовать fit и ширину колонок в % если колонок не много(4-5),
   * expand ширина колонок в px
   */
  public gridResizeMode: 'fit' | 'expand' = 'expand';
  public first: number = 0;

  // public gridContextMenu = [
  //   {
  //     label: 'Open in new tab',
  //     icon: 'pi pi-search',
  //     command: (event) => {
  //       // console.log(this.selectedItem);
  //       this.newTabService.openInNewTab(this.fullCardRoute);
  //     }
  //   }
  // ];

  // public buttonsDisabledStatus: { [key: string]: boolean } = {
  //   edit: null,
  //   new: null,
  //   delete: null,
  //   cloneCard: null,
  //   refresh: null,
  //   exportToExcel: null,
  //   resetStoredSettings: null,
  //   newTab: null
  // };

  // protected menuPresets: { [key: string]: Array<string> } = {
  //   default: ['new', 'delete', 'edit', 'cloneCard', 'filter', 'refresh', 'exportToExcel', 'resetStoredSettings', 'newTab']
  // };

  protected localStorageKey: string = null;

  // /*
  //  * gridRoute роут грида, нужен для открытия вложенного грида в новом окне
  //  * cardRoute роут карточки
  //  * fullCardRoute роут карточки с id
  //  */
  // protected gridRoute: string = null;
  // protected cardRoute: string = null;
  // protected fullCardRoute: string = null;

  // private _cardId: number = null;


  constructor(
    protected newTabService: NewTabService
  ) { }

  // protected setGridName(name: string): void {
  //   this.gridName = name;
  // }

  /* gridCode нужен для ключа грида в ls*/
  protected setGridCode(elem: ElementRef): void {
    if (elem) {
      this.gridCode = elem.nativeElement.localName;
      this.localStorageKey = `gridSettings: ${this.gridCode}`;
    }
  }

  // protected fillGridMenu(menuSettings: string[]): void {
  //   menuSettings.forEach(buttonName => {
  //     if (this.buttons[buttonName]) {
  //       this.gridMenu.push(this.buttons[buttonName]);
  //     }
  //   });
  // }

  // protected setButtonsDisabledStatus(disabledButtons: Array<string>, disabled: boolean = true): void {
  //   disabledButtons.forEach(button => {
  //     if (this.buttonsDisabledStatus.hasOwnProperty(button)) {
  //       this.buttonsDisabledStatus[button] = disabled;
  //     }
  //   });
  // }


  // /**
  //  * для добавления статуса дополнительных кнопок грида
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-04-24
  //  * @protected
  //  * @param {{}} objectStatus
  //  * @memberof GridSettings
  //  */
  // protected assignButtonsDisabledStatus(objectStatus: {}): void {
  //   Object.assign(this.buttonsDisabledStatus, objectStatus);
  // }

  // // Задать колонку типа текст
  protected textColumn(column: GridColumn): GridColumn {
    const textColumn: GridColumn = column;
    textColumn.type = 'text';
    textColumn.textAlign = textColumn.textAlign ? textColumn.textAlign : 'center';
    return this.formatColumn(textColumn);
  }
  protected funcColumn(column: GridColumn): GridColumn {
    const textColumn: GridColumn = column;
    textColumn.type = 'func';
    textColumn.textAlign = textColumn.textAlign ? textColumn.textAlign : 'left';
    return this.formatColumn(textColumn);
  }

  // Задать колонку типа число (с указанием количества знаков после запятой)
  protected numericColumn(column: GridColumn): GridColumn {
    const numericColumn: GridColumn = column;
    numericColumn.type = 'number';
    numericColumn.textAlign = numericColumn.textAlign ? numericColumn.textAlign : 'right';
    return this.formatColumn(numericColumn);
  }

  // Задать колонку типа сумма (два знака после запятой)
  protected moneyColumn(column: GridColumn): GridColumn {
    const moneyColumn: GridColumn = column;
    moneyColumn.type = 'money';
    moneyColumn.textAlign = 'right';
    return this.formatColumn(moneyColumn);
  }

  // Задать колонку с округлением числа до определенногокол-ва знаков после запятой
  protected roundColumn(column: GridColumn): GridColumn {
    const roundColumn: GridColumn = column;
    roundColumn.type = 'round';
    roundColumn.textAlign = 'right';
    return this.formatColumn(roundColumn);
  }

  protected dateColumn(column: GridColumn): GridColumn {
    const dateColumn: GridColumn = column;
    dateColumn.type = 'date';
    dateColumn.textAlign = dateColumn.textAlign ? dateColumn.textAlign : 'center';
    return this.formatColumn(dateColumn);
  }

  protected dateTimeColumn(column: GridColumn): GridColumn {
    const dateTimeColumn: GridColumn = column;
    dateTimeColumn.type = 'datetime';
    dateTimeColumn.textAlign = dateTimeColumn.textAlign ? dateTimeColumn.textAlign : 'center';
    return this.formatColumn(dateTimeColumn);
  }

  // Колонка типа чекбокс
  protected checkBoxColumnYN(column: GridColumn): GridColumn {
    const checkBoxColumn: GridColumn = column;
    checkBoxColumn.type = 'checkboxYN';
    checkBoxColumn.textAlign = checkBoxColumn.textAlign ? checkBoxColumn.textAlign : 'center';
    return this.formatColumn(checkBoxColumn);
  }

  // Колонка типа цветная кнопка
  protected colorColumn(column: GridColumn): GridColumn {
    const colorColumn: GridColumn = column;
    colorColumn.type = 'color';
    colorColumn.textAlign = colorColumn.textAlign ? colorColumn.textAlign : 'center';
    return this.formatColumn(colorColumn);
  }

  private formatColumn(column: GridColumn): GridColumn {
    column.width = column.width ? column.width : '100px';
    column.visible = column.visible ? column.visible : true;
    column.precision = column.precision ? column.precision : 0;
    column.decimal = column.decimal ? column.decimal : ',';
    column.accuracu = column.accuracu ? column.accuracu : 2;
    column.noZerro = column.noZerro ? column.noZerro : false;
    column.func = column.func ? column.func : null;
    column.funcParam = (column.funcParam && column.funcParam.length > 0) ? column.funcParam : null;
    return column;
  }

}
