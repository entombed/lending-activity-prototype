import { OnInit, HostListener, Output, EventEmitter, ElementRef, Renderer2, AfterViewInit, Input, OnDestroy } from '@angular/core';
import { AnimationEvent } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MessageInfoService } from '@beua-services/message-info.service';
import { DictionaryClient } from '@beua-services/web-api/pcm/pcm.web-api.services';
import { DictionaryClientWithCache } from '@beua-services/dictionary.service';
import { CalendarLang } from '@beua/shared/calendar-lang';

export interface CardFlags {
  editingModeOff: boolean;
  fieldsAreLocked: boolean;
  options?: object;
}

export class FilterBase implements OnInit, AfterViewInit, OnDestroy {

  @Output() changeFilter: EventEmitter<any> = new EventEmitter<any>();
  public additive = '';
  public showCloseWindow;
  public setCardClosed;
  public animatoState = 'visible'; // указываем состояние анимации карточки
  public flags: CardFlags = {
    editingModeOff: true, // карточка в режиме редактирования или нет
    fieldsAreLocked: true, // карточка в режиме редактирования или нет
  };
  public filterTitle = ''; // заголовок фильтра
  public dictionaries: object[] = []; // словари для выпадающих списков
  protected bodyElement: HTMLBodyElement; // сохраняем body
  protected countModalWindows: number; // кол-во открытых модальных окон в body
  public showCardContent = false;
  protected lang: string; // язык интерфейса
  public statusCollapse = false; // свернуть или развернуть блоки по кнопке
  protected i18nWords: string[] = []; // массив слов для которых необходимо получить перевод
  protected translations: {} = {}; // словарик с переведенными словами
  // используется для добавления и последующей отписки при удаленние компонента (this.subscription.add())
  protected subscription: Subscription = new Subscription();
  // отслеживаем изменения фильтра
  public filter: any;
  public parentNameGrid: string = null; // название тега грида, с которого вызван фильтр. Приходит как свойство paramsFilter, но на каждом фильтре надо его присваивать
  public calendarLang: any;
  public currentLang: any; // язык для календаря

  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageInfoService: MessageInfoService,
    protected translateService: TranslateService,
    protected router: Router,
    protected dictionaryClient?: DictionaryClient,
  ) {
    this.lang = this.translateService.currentLang; // запоминаем язык интерфейса пользователя
    this.calendarLang = new CalendarLang();
    this.currentLang = this.calendarLang[this.lang]; // язык для календаря
  }

  ngOnInit() {
      this.flags.editingModeOff = false;
      this.flags.fieldsAreLocked = false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  protected listDictionary(list: string, withoutCode: boolean = false, withoutFirstEmptyRow: boolean = false, success?: () => void) {
    (this.dictionaryClient as DictionaryClientWithCache).listDictionary(
      this.dictionaries,
      list,
      withoutCode,
      withoutFirstEmptyRow,
      success
    );
  }

  // даем понять, что параметры фильтрации изменились
  public chooseFilterParam() {
    this.changeFilter.emit(this.filter);
  }

  public setFilterParams(params?: any) {
    if (params && this.filter) {
        for (const key in params) {
            if (this.filter.hasOwnProperty(key)) {
                const proto = this.filter[key].__proto__;
                this.filter[key] = params[key];
                this.filter[key].__proto__ = proto;
            }
        }
        // передаем имя родителького грида для шаблонов пользователя
        if (params.hasOwnProperty('parentNameGrid')) {
            this.parentNameGrid = params.parentNameGrid;
        }
    }
  }

  /**
   * сохраням в переменную body
   * подсчитываем кол-во открытых карточек в момент создания компонента
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  ngAfterViewInit() {
    this.bodyElement = this.renderer.selectRootElement('body', true);
    this.countModalWindows = this.bodyElement.querySelectorAll('.be-modal-card-overlay').length;
    this.disableBodyScroll();
  }

  /**
   * отлавливаем нажатие ESC
   * смотрим у какого компонента на странице кол-во карточек === 1
   * считаем что это самая последняя открытая карточка, запускаем закрытие этой карточки
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {KeyboardEvent} event
   * @memberof BaseCardComponent
   */
  // @HostListener('document:keydown.Escape', ['$event'])
  // onEscapePress(event: KeyboardEvent) {
  //   const modal = this.elementRef.nativeElement.querySelectorAll('.be-modal-card-overlay').length;
  //   if (modal === 1) {
  //     this.startCloseCard();
  //   }
  // }

  /**
   *
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string[]} [arrayWords=this.i18nWords] - массив слов которые необходимо перевести
   * @param {(() => void)} [callBack] - callback функция
   * @memberof BaseCardComponent
   */
  protected getTranslates(arrayWords: string[] = this.i18nWords) {
    return this.translateService.get(arrayWords).pipe(
      map((translatedWords) => {
        this.translations = translatedWords;
        return translatedWords;
      })
    );
  }

  /**
   * отключаем скрол на body
   * для того что бы предотвратить появления второго скрола
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @private
   * @memberof BaseCardComponent
   */
  private disableBodyScroll() {
    // if (this.bodyElement.style.overflow !== 'hidden') {
    //   this.renderer.setStyle(this.bodyElement, 'overflow', 'hidden');
    // }
  }

  public closeForm() {
    // this.closeFilter.emit();
    this.changeFilter.emit({ closeForm: true });
  }

  /**
   * при закрытии карточки запускаем анимацию
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  startCloseCard() {
      this.changeAnimationState('close');
  }

  /**
   * изменяем состояния анимации
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} state
   * @memberof BaseCardComponent
   */
  public changeAnimationState(state) {
    this.animatoState = state;
  }


  /**
   * закрываем все открытые акардионы на странице
   * выбираем все открытыте акардионы и закрываем их
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  public collapseOpennedAccordions(event) {
    const setCollapse = event.checked;
    let listAccordion = null;
    if (setCollapse) {
      listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header.ui-state-active > a');
    } else {
      listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header:not(.ui-state-active) > a');
    }
    listAccordion.forEach((element: HTMLElement) => {
      this.renderer.selectRootElement(element, true).click();
    });
  }

  /**
   * делаем поля формы доступными для редактирования или нет
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} mode
   * @memberof BaseCardComponent
   */
  public setLockFields(mode) {
    this.flags.editingModeOff = mode;
  }

  /**
   * запускаем после завершения анимации
   * закрытие карточки
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} event
   * @memberof BaseCardComponent
   */
  public animationDone(event: AnimationEvent) {
    if (event.toState === 'close') {
      // this.setCardClosed(event);
    }
  }

  /**
   * задаем заголовок фильтра
   *
   * @author A.Bondarenko
   * @date 2020-04-14
   * @protected
   * @param {string} name
   * @memberof CardBase
   */
  protected setFilterTitle(name: string): void {
    this.filterTitle = name;
  }

  public clearForm() {
    const keys = ['value', 'desc', 'values', 'valueBegin', 'valueEnd', 'extValue', 'items'];
    Object.keys(this.filter).forEach((item) => {
      this.filter[item].isFind = false;
      console.log(this.filter[item].constructor.name)
      keys.forEach((key) => {
        if (key in this.filter[item]) {
          if (key === 'values') {
            this.filter[item][key] = {};
          } else if (key === 'valueBegin') {
            this.filter[item].valueBegin = null;
            this.filter[item].valueEnd = null;
          } else if (key === 'items') {
            this.filter[item][key] = [];
          } else {
            this.filter[item][key] = null;
          }
        }
      });
    });
    this.createSearchPickerOptions();
  }

  protected createSearchPickerOptions() {}

}
