import { GridBase } from './grid-base';
import { OnInit, AfterViewInit, Output, Input, EventEmitter, ElementRef, Renderer2 } from '@angular/core';
import { CardFlags } from './card-base';
import { Router } from '@angular/router';
import { NewTabService } from '@beua-services/new-tab.service';
import { MessageInfoService } from '@beua-services/message-info.service';
import { TranslateService } from '@ngx-translate/core';
import { ODataServiceBase } from '@beua-services/odata/odataservicebase';
import { ODataCompareOperator, ODataComparison, ODataProperty, ODataValue, IODataPredicate } from '@beua-services/odata/odataquery';
import { SelectionModeGrid } from './grid-settings';

export interface SelectedHandbookItem {
  cancel: boolean;
  data: any;
}

export class SelectBase<O> extends GridBase<O> implements OnInit, AfterViewInit {

  @Input() codeName: string = null;
  @Input() additive = ''; // примесь для формирования уникальных ID в DOM
  @Input() queryType: string = null; // тип запроса, используется чтобы добавить условия выборки
  @Input() existItems: any[] = []; // массив из уже выбранных элементов, они будут исключены из запроса
  @Input() clarification: string = null; // использутся для расширенных запросов, определяет тип запроса
  @Input() additionalConditions: any = null; // параметры для построения расширенных запросов
  @Output() closeSelectForm: EventEmitter<SelectedHandbookItem> = new EventEmitter(); // отдаем на родителя найденный объект

  public flags: CardFlags = {
    editingModeOff: true, // карточка в режиме редактирования или нет
    editingModeOn: false, // карточка в режиме редактирования или нет
    isNewRecord: false, // карточка в режиме создания новой записи или редактирования существующей
    isHistoryVisible: false,
    isHistoryBlockVisible: false
  };
  protected gridColumns = [];
  public initialPaginationParams = { first: 0, rows: 30 }; // кл-во строк в таблице
  public selectionMode: SelectionModeGrid = 'single';
  constructor(
    protected elem: ElementRef,
    protected router: Router,
    protected newTabService: NewTabService,
    protected abstractOdataService: ODataServiceBase<O>,
    protected messageInfoService: MessageInfoService,
    protected translateService: TranslateService,
    protected renderer: Renderer2
  ) {
    super(elem, router, newTabService, abstractOdataService, messageInfoService, translateService, renderer);
  }

  ngOnInit() {
    this.gridIsOpenOn = 'onModal'; // указываем что у нас грид открывается в модальном окне
    // this.setGridResizeMode('fit'); // задаем поведение колонок справочника
    this.createCols(); // создаем колонки
    this.initColumns(this.gridColumns); // инициируем колонки
    this.loadData(); // загружаем данные для таблицы
  }

  /**
   * заглушка, Заполняем колонки для гриде
   *
   * @author A.Bondarenko
   * @date 2020-05-11
   * @protected
   * @memberof SelectBase
   */
  protected createCols(): void { }

  /**
   * нажатие кнокпи выбрать
   * отправляем данные на родителя
   * cancel: false - говорим что закрыли форму и выбрали данные
   *
   * @author A.Bondarenko
   * @date 2020-05-11
   * @memberof SelectBase
   */
  public selectItem(): void {
    if (this.selectedItem != null && !Array.isArray(this.selectedItem)) {
      this.selectedItem = [this.selectedItem];
    }
    this.closeSelectForm.emit({
      cancel: false,
      data: this.selectedItem[0]
    });
  }

  /**
   * нажатие кнопки закрыть
   * отправляем на родителя объект с признаком что данные не выбраты
   * и форму закрыли по кнопке cancel (cancel: true)
   *
   * @author A.Bondarenko
   * @date 2020-05-11
   * @memberof SelectBase
   */
  public closeForm(): void {
    this.closeSelectForm.emit({
      cancel: true,
      data: null
    });
  }


  /**
   * Очистки фильтра
   *
   * @author A.Bondarenko
   * @date 2020-05-11
   * @memberof SelectBase
   */
  public clearFilter(): void {
    this.codeName = null;
    this.selectedItem = null;
    this.reloadData();
  }

  /**
   * нажатие кнопки фильтр
   *
   * @author A.Bondarenko
   * @date 2020-04-17
   * @memberof SelectBase
   */
  public startFitering(): void {
    this.selectedItem = null;
    this.reloadData();
  }

  /**
   * обрабатываем двойное нажатие на строчке в таблице
   *
   * @author A.Bondarenko
   * @date 2020-05-11
   * @param {*} event
   * @param {O} rowData
   * @memberof SelectBase
   */
  public rowDblClickHandler(event, rowData: O): void {
    this.selectedItem = [rowData];
    if (rowData) {
      this.selectItem();
    }
  }

  /**
   * @author A.Bondarenko
   * @date 2020-05-12
   * @protected
   * @param {string} nameFild - поле таблицы по которому проводим поиск
   * @param {string} dataFild - поле в массиве data[] из которого мы берем значения
   * @param {ODataCompareOperator} comparisonOperator опратор условия (=, != и т.д)
   * @param {any[]} data - массив данных которые используются для условия
   * @returns {IODataPredicate[]}
   * @memberof SelectBase
   */
  protected createPredicates(
    nameFild: string,
    dataFild: string,
    comparisonOperator: ODataCompareOperator,
    data: any[]
  ): IODataPredicate[] {
    const predicates = data.map((item) => {
      const queryItem = new ODataComparison(
        new ODataProperty(nameFild),
        comparisonOperator,
        new ODataValue(item[dataFild])
      );
      return queryItem;
    });
    return predicates;
  }
}
