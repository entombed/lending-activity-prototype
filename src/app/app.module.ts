import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TranslateModule, TranslateLoader, MissingTranslationHandlerParams, MissingTranslationHandler } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PrimengSharedModule } from './Common/shared/primeng-shared.module';
import { FormsModule } from '@angular/forms';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json?id=20200717');
}

/**
 * отлавливаем когда нет перевода
 * выводим сообщение в консоль только в режиме разработки
 *
 * @author A.Bondarenko
 * @date 2020-01-28
 * @export
 * @class MissingTranslationService
 * @implements {MissingTranslationHandler}
 */
export class MissingTranslationService implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    // if (!environment.production) {
    //   console.warn(`WARN: Key '${params.key}' is missing in ${params.translateService.currentLang}.json file`);
    // }
    console.warn(`WARN: Key '${params.key}' is missing in ${params.translateService.currentLang}.json file`);
    return params.key;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MissingTranslationService
      },
    }),
    PrimengSharedModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
