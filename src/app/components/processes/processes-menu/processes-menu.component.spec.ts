import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessesMenuComponent } from './processes-menu.component';

describe('ProcessesMenuComponent', () => {
  let component: ProcessesMenuComponent;
  let fixture: ComponentFixture<ProcessesMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessesMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessesMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
