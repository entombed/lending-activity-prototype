import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-processes-menu',
  templateUrl: './processes-menu.component.html',
  styleUrls: ['./processes-menu.component.scss']
})
export class ProcessesMenuComponent implements OnInit {


  items: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.items = [
      {
        label: 'ADD',
        items: [
          { label: 'Add User', icon: 'pi pi-fw pi-user-plus' },
          { label: 'Remove User', icon: 'pi pi-fw pi-user-minus' }
        ]
      },
      {
        label: 'EDIT',
        items: [
          { label: 'Add User', icon: 'pi pi-fw pi-user-plus' },
          { label: 'Remove User', icon: 'pi pi-fw pi-user-minus' }
        ]
      },
      {
        label: 'DELETE',
        items: [
          { label: 'Add User', icon: 'pi pi-fw pi-user-plus' },
          { label: 'Remove User', icon: 'pi pi-fw pi-user-minus' }
        ]
      }];
  }

}
