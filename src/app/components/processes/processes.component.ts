import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-processes',
  templateUrl: './processes.component.html',
  styleUrls: ['./processes.component.scss']
})
export class ProcessesComponent implements OnInit {
  items: any = null
  constructor() { }

  ngOnInit(): void {
    this.prepareItemMenu()
  }

  prepareItemMenu() {
    this.items = [
      {
        name: 'PM Corporate',
        link: null,
        visible: true,
        children: [
          {
            name: 'MONITORING',
            link: './pm-corporate-monitoring',
            visible: true,
          },
          {
            name: 'RISK_ASSESSMENT',
            link: './risk-assessment',
            visible: true,
          }
        ]
      },
      {
        name: 'PM (Retail) 111',
        link: './assessment111',
        visible: true,
        children: []
      },
      {
        name: 'PM (Retail) 222',
        link: './assessment222',
        visible: false,
        children: []
      },
      {
        name: 'PM (Retail) 333',
        link: './assessment333',
        visible: true,
        children: [
          {
            name: 'MONITORING',
            link: './pm-corporate-monitoring',
            visible: true,
          },
          {
            name: 'RISK_ASSESSMENT',
            link: './',
            visible: true,
          }
        ]
      }
    ]
  }
}

