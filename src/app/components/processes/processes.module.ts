import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClarityModule } from '@clr/angular';

import { ProcessesRoutingModule } from './processes-routing.module';
import { ProcessesComponent } from './processes.component';
import { PrimengSharedModule } from 'src/app/Common/shared/primeng-shared.module';
import { ComponetsSharedModule } from 'src/app/Common/shared/componets-shared.module';
import { ProcessesMenuComponent } from './processes-menu/processes-menu.component';
import { ExapmleGridProcessesModule } from '../exapmle-grid-processes/exapmle-grid-processes.module';


@NgModule({
  declarations: [
    ProcessesComponent,
    ProcessesMenuComponent
  ],
  imports: [
    CommonModule,
    ProcessesRoutingModule,
    PrimengSharedModule,
    ComponetsSharedModule,
    ClarityModule,
    ExapmleGridProcessesModule
  ]
})
export class ProcessesModule { }
