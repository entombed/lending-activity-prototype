import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessesComponent } from './processes.component';
import { ExapmleGridProcessesComponent } from '../exapmle-grid-processes/exapmle-grid-processes.component';


const routes: Routes = [
  {
    path: '',
    component: ProcessesComponent,
    children: [{
      path: 'exapmle-grid',
      component: ExapmleGridProcessesComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessesRoutingModule { }
