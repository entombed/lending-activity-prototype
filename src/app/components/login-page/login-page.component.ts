import { Component, OnInit } from '@angular/core';
// import { UserService, User } from '@beua-services/user/user.service';
import { Router, ActivatedRoute } from '@angular/router';
// import { MessageInfoService } from '@beua-services/message-info.service';

class NewPass {
  userName: string = '';
  oldPassword: string = '';
  newPassword: string = '';
  newPasswordConfirm: string = '';
}

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnInit {

  // public user: User;
  public user: any;
  public isLoginForm: boolean = true;
  public newPass: NewPass;
  public showPassword: boolean = false;

  constructor(
    // private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    // private messageInfoService: MessageInfoService
  ) { }

  ngOnInit() {
    // this.user = new User();
    this.user = {
      username: null,
      password: null
    };
  }

  public loginButtonHandler(): void {
    // this.checkUserCredentials();
    this.router.navigate(['App/processes']);

  }

  public changePasswordButtonHandler(): void {
    this.newPass = new NewPass();
    this.newPass.userName = this.user.username;
    this.changeForm();
  }

  public confirmPasswordChangeButtonHandler(): void {
    // if (this.newPass.newPassword === this.newPass.newPasswordConfirm) {
    //   this.changeUserPassword();
    // } else {
    //   this.messageInfoService.showErrorMessage('PASSWORD.DONT_MATCH');
    // }
  }

  public cancelPasswordChangeButtonHandler(): void {
    this.newPass = new NewPass();
    this.changeForm();
  }

  public newPassFormNotFilled(): boolean {
    return Object.values(this.newPass).some(val => val === '');
  }

  public passwordDisplayType(): string {
    return this.showPassword ? 'text' : 'password';
  }

  private checkUserCredentials(): void {
    // this.userService.registerAuth(this.user, () => this.successfulLogin(), (error) => this.messageInfoService.showErrorMessage(error));
  }

  private changeUserPassword(): void {
    // this.userService.changePassword(this.newPass.userName, this.newPass.oldPassword, this.newPass.newPassword,
    //   (error) => this.handlePasswordChangeRequest(error));
  }

  private successfulLogin(): void {
    // const route = this.route.snapshot.queryParams.route;
    // route ? this.router.navigate([`${route}`]) : this.navigateToHomePage();
  }

  private handlePasswordChangeRequest(error): void {
    // if (error) {
    //   this.messageInfoService.showErrorMessage(error);
    // } else {
    //   this.messageInfoService.showSuccessMessage('PASSWORD_IS_CHANGED');
    //   this.changeForm();
    // }
  }

  private navigateToHomePage(): void {
    this.router.navigate(['App/RefBooks']);
  }

  private changeForm(): void {
    this.isLoginForm = !this.isLoginForm;
  }

}
