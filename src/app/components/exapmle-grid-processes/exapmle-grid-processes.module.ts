import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExapmleGridProcessesComponent } from './exapmle-grid-processes.component';



@NgModule({
  declarations: [ExapmleGridProcessesComponent],
  imports: [
    CommonModule
  ],
  exports: [
    ExapmleGridProcessesComponent
  ]
})
export class ExapmleGridProcessesModule { }
