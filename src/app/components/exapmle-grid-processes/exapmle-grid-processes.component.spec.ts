import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExapmleGridProcessesComponent } from './exapmle-grid-processes.component';

describe('ExapmleGridProcessesComponent', () => {
  let component: ExapmleGridProcessesComponent;
  let fixture: ComponentFixture<ExapmleGridProcessesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExapmleGridProcessesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExapmleGridProcessesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
