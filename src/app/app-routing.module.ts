import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/login-page/login-page.component';

const routes: Routes = [
  {
    path: 'App',
    children: [
      { path: '', component: LoginPageComponent },
      {
        path: 'processes',
        loadChildren: () => import('./components/processes/processes.module').then(m => m.ProcessesModule)
      },
      { path: '**', component: LoginPageComponent }
    ]
  },
  { path: '', redirectTo: '/App', pathMatch: 'full' },
  { path: '**', redirectTo: '/App' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
